package gradle.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.PendingException;
import static org.junit.Assert.*;

public class BasicStepsdefs {

  @Given("A failing scenario")
  public void a_failing_scenario() {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
    //throw new AssertionError();
    assertEquals("true", "true");
  }

  @When("I run a failing step")
  public void i_run_a_failing_step() {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
    assertTrue(true);
  }

  @Then("I got a failing step")
  public void i_got_a_failing_step() {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
    assertEquals(100, 100);
  }
}